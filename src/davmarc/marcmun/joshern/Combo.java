/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package davmarc.marcmun.joshern;

/**
 *
 * @author chema
 */
import java.util.ArrayList;

public class Combo extends Producto {
    
    //Alamacena los productos en el combo
    private ArrayList<Producto> productos = new ArrayList<>();

    /**
     * Añade un producto al combo
     *
     * @param p Producto que se añade al combo
     */
    public void anadeProducto(Producto p) {
        productos.add(p);
    }

    /**
     * Crea nuevo Combo
     *
     * @param productos arraylist de productos del nuevo combo
     * @param descripcion string con la descripcion del combo
     */
    public Combo(String nombre, String descripcion) {
//        this.productos = productos;
        this.setNombre(nombre);
        this.setDescripcion(descripcion);
    }

    /**
     * Elimina un producto del combo
     *
     * @param p Producto que se saca al combo
     */
    public void eliminaProducto(Producto p) {
        productos.remove(p);
    }

    /**
     * Devuelve cada producto del combo
     *
     * @return Producto[] lista de productos del combo
     */
    public Producto[] getProductos() {
        Producto[] productosAux = new Producto[productos.size()];
        productos.toArray(productosAux);
        return productosAux;
    }

    /**
     * Devuelve el numero de calorias del combo
     *
     * @return sumaCalorias float que contiene el numero de calorias
     */
    @Override
    public float getNumCalorias() {
        
        float sumaCalorias = 0;
        
        for (Producto plato : productos) {
            sumaCalorias += plato.getNumCalorias();
        }
        return sumaCalorias;
    }

    /**
     * Devuelve el precio del combo
     *
     * @return sumaPrecios float que contiene el numero que corresponde al
     * precio dl combo
     */
    @Override
    public float getPrecio() {
        
        float sumaPrecios = 0;
        
        for (Producto plato : productos) {
            sumaPrecios += plato.getPrecio();
        }
        sumaPrecios = sumaPrecios * 0.8f;
        return sumaPrecios;
    }
}
