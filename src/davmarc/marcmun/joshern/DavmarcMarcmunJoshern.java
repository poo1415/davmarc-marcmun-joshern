/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package davmarc.marcmun.joshern;

/**
 *
 * @author David Marciel
 * @author Marcos Muñoz
 * @author Jose María HernamPerez
 */
public class DavmarcMarcmunJoshern {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //crea e imprime la carta
        UsaProductosInfoBurguer carta = new UsaProductosInfoBurguer();
        carta.creaCarta();
        System.out.print(carta.imprimeCarta(carta.getCarta()));
        
        //muestra información sobre la hamburguesa
        muestra("Las calorias de una hamburguesa son: " + carta.get(0).getNumCalorias());
        muestra("El precio de una hamburguesa es: " + carta.get(0).getPrecio());
        muestra("La informacion de una hamburguesa es: " + carta.get(0).getInfo());
        muestra("");
        
        //muestra información sobre nestea
        muestra("Las calorias de un nestea son: " + carta.get(2).getNumCalorias());
        muestra("El precio de un nestea es: " + carta.get(2).getPrecio());
        muestra("La informacion de un nestea es: " + carta.get(2).getInfo());
        muestra("");
                
        //muestra información sobre las patatas
        muestra("Las calorias de un patatas son: " + carta.get(1).getNumCalorias());
        muestra("El precio de un pattas es: " + carta.get(1).getPrecio());
        muestra("La informacion de un patatas es: " + carta.get(1).getInfo());
        muestra("");
        
        //muestra información sobre el comboPatatas
        muestra("Las calorias de un comboPatatas son: " + carta.get(4).getNumCalorias());
        muestra("El precio de un comboPattas es: " + carta.get(4).getPrecio());
        muestra("La informacion de un comboPatatas es: " + carta.get(4).getInfo());
        muestra("");
        
    }
    
    /**
     * Metodo encargado de mostrar por pantalla
     * @param String s a mostrar por pantalla 
     */
    public static void muestra(String s){
        System.out.println(s);
    }
}