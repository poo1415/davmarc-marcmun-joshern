/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package davmarc.marcmun.joshern;

/**
 *
 * @author chema
 */
import java.util.ArrayList;

/**
 *
 * @author david
 */
public class Hamburguesa extends Producto {

    /**
     * Crea nueva hamburguesa
     *
     * @param ingredientes arraylist de ingrediente de la hamburguesa
     * @param calorias entero con numero de calorias de la hamburguesa
     * @param nombre string con el nombre de la hamburguesa
     * @param precio double con el precio de la hamburguesa
     */
    Hamburguesa(String descripcion, float calorias, String nombre, float precio) {
        setPrecio(precio);
        setNombre(nombre);
        setNumCalorias(calorias);
        setDescripcion(descripcion);
    }
    //Hamburguesa: Para ellas debemos modelar además una 
    //lista de ingredientes (expresados simplementes como cadenas de caracteres):
    private ArrayList<String> ingredientes = new ArrayList<>();

    /**
     * Añade un ingrediente a la hamburguesa
     *
     * @param i String con nombre de la hamburguesa
     */
    public void anadeIngredientes(String i) {
        ingredientes.add(i);
    }

    /**
     * Elimina un ingrediente a la hamburguesa
     *
     * @param i String con nombre de la hamburguesa
     */
    public void eliminaIngrediente(String i) {
        ingredientes.remove(i);
    }

    /**
     * Devuelve cada ingrediente a la hamburguesa
     *
     * @return String[] con los ingredientes de la hamburguesa
     */
    public String[] getIngredientes() {
        String[] ingredientesAux = new String[ingredientes.size()];
        ingredientes.toArray(ingredientesAux);
        return ingredientesAux;
    }

    /**
     * Devuelve la informacion de la hamburguesa
     * @return String informacion
     */
    @Override
    public String getInfo() {
        String s = super.getInfo();
        
        for (String ingrediente : ingredientes) {
            s += " "+ ingrediente;
        }        
        return s;
    }
    
    
}