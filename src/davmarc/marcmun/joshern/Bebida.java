/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package davmarc.marcmun.joshern;

/**
 *
 * @author chema
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
public class Bebida extends Producto {
    
    //propiedades de la bebida
    private boolean sinCafeina;
    private boolean sinAzucar;

    /**
     * Crea nueva hamburguesa
     *
     * @param sinCafeina true crea la bebida sin cafeina false en caso contrario
     * @param sinAzucar true crea la bebida sin azucar false en caso contrario
     * @param calorias entero con numero de calorias de la Bebida
     * @param nombre string con el nombre de la Bebida
     * @param precio double con el precio de la Bebida
     */
    public Bebida(String descripcion, boolean sinCafeina, boolean sinAzucar, float calorias, String nombre, float precio) {
        setPrecio(precio);
        setNombre(nombre);
        setNumCalorias(calorias);
        setDescripcion(descripcion);
        this.sinCafeina = sinCafeina;
        this.sinAzucar = sinAzucar;
    }

    /**
     * Dice si es na bebida cafeinica
     *
     * @return true si la bebida no contiene cafeina
     */
    public boolean isSinCafeina() {
        return sinCafeina;
    }

    /**
     ** Dice si es na bebida cafeinica
     *
     * @return true si la bebida no contiene azucar
     */
    public boolean isSinAzucar() {
        return sinAzucar;
    }

    /**
     * Establece si la bebida es con cafeina
     *
     * @param sinCafeina valor booleano que da cafeina a la bebida si es false y
     * se la quita si es true
     */
    public void setSinCafeina(boolean sinCafeina) {
        this.sinCafeina = sinCafeina;
    }

    /**
     * Establece si la bebida es con cafeina
     *
     * @param sinAzucar valor booleano que da azucar a la bebida si es false y
     * se la quita si es true
     */
    public void setSinAzucar(boolean sinAzucar) {
        this.sinAzucar = sinAzucar;
    }
}
