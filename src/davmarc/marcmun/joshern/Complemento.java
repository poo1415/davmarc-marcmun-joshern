/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package davmarc.marcmun.joshern;
/**
 *
 * @author chema
 */
public class Complemento extends Producto{
    //Complemento: Los complementos (ensalada, patatas,...) 
    //están diseñados para un número determinado de comensales.
    private int numComensales;

    /**
     * Constructor de Producto
     * 
     * @param descripcion
     * @param nombre
     * @param precio
     * @param calorias
     * @param nComensales 
     */
    Complemento(String descripcion,String nombre, float precio, float calorias,int nComensales) {
        this.setNombre(nombre);
        this.setNumCalorias(calorias);
        this.setPrecio(precio);
        setDescripcion(descripcion);
        numComensales=nComensales;
        
    }
    /**
     * Devuelve la información asociada al complemento
     * @return String informacion
     */
    @Override
    public String getInfo() {
        return  getNombre()+":\n  "
                + "  Precio:" + this.getPrecio()
                + "  Calorias:" + getNumCalorias()
                + "  Descripcion:" + this.getDescripcion()
                + "  Numero de  Comnensales:"+numComensales;
    }
}

