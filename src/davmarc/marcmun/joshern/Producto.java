/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package davmarc.marcmun.joshern;

/**
 *
 * @author david
 */
/**
 *
 * @author chema
 */
public abstract class Producto {
    
    //Atributos de producto
    private String nombre;
    private String descripcion;
    private float precio;
    private float numCalorias;

    /**
     * Devuelve la información del producto
     *
     * @return String informacion de producto
     */
    public String getInfo() {
        return nombre + ":\n  "
                + "  Precio:" + this.getPrecio()
                + "  Calorias:" + getNumCalorias()
                + "  Descripcion:" + this.getDescripcion();
    }

    /**
     * Devuelve el precio del producto
     *
     * @return precio float que contiene el numero que corresponde al precio
     */
    public float getPrecio() {
        return precio;
    }

    /**
     * Establece el precio del producto
     *
     * @param numCalorias float que contiene el numero que pasa a ser el precio
     * del producto
     */
    public void setPrecio(float precio) {
        this.precio = precio;
    }

    /**
     * Devuelve el nombre del producto
     *
     * @return nombre string que contiene el nombre del producto
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Establece el nombre del producto
     *
     * @param nombre pasa a ser el nuevo nombre del producto
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Devuelve el numero de calorias del producto
     *
     * @return numCalorias float que contiene el numero de calorias
     */
    public float getNumCalorias() {
        return numCalorias;
    }

    /**
     * Establece el numero de calorias del producto
     *
     * @param numCalorias float que contiene el numero que pasan a ser las
     * calorias del producto
     */
    public void setNumCalorias(float numCalorias) {
        this.numCalorias = numCalorias;
    }
        /**
     * Devuelve la descripcion del producto
     * 
     * @return descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * Añade la descripcion del producto
     * 
     * @param descripcion del producto
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
