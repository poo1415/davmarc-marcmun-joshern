/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package davmarc.marcmun.joshern;

/**
 *
 * @author chema
 */
 class UsaProductosInfoBurguer {

     //Encargado de guardar la información de la carta
     private Producto[] carta;
     
     /**
      * Genera e inicializa los productos de la carta
      */
    public void creaCarta() {

        carta = new Producto[6];
        
        //rellena la carta
        carta[0] = hamburguesaDoble();
        carta[1] = patatas();
        carta[2] = creaNestea();
        carta[3] = creaCocacola();
        carta[4] = comboPatatas();
        carta[5] = comboPatatasCocacola();    
    }

    /**
     * Genera cocaCola
     * @return Producto cocaCola
     */
    private static Bebida creaCocacola() {
        
        //crea las bebidas cocacola
        return new Bebida("bebida de cola", false, false, 35.5f, "cocacola", 1.2f);
    }

    /**
     * Genera nestea
     * @return Producto nestea
     */
    private static Bebida creaNestea() {
        
        //crea las bebidas nestea
        return new Bebida("bebida de te", true, false, 35.5f, "nestea", 1.4f);
    }

    /**
     * Genera HamburguesaDoble
     * @return Producto HamburguesaDoble
     */
    private Producto hamburguesaDoble() {
    
        //crea hamburguesa 
        Hamburguesa ham = new Hamburguesa("dos hamburguesas", 39f, "doble chesburger", 4.5f);
        ham.anadeIngredientes("carne");
        ham.anadeIngredientes("queso");
        ham.anadeIngredientes("queso");

        return ham;
    }

    /**
     * Genera patatas
     * @return Producto patatas
     */
    private Producto patatas() {
        
        //crea el complemento patatas
        Complemento patatas = new Complemento("plato de patatas", "patatas", 3f, 55f, 2);
        
        return patatas;
    }
    
    /**
     * Genera comboPatatas
     * @return Producto comboPatatas
     */
    private Producto comboPatatas() {
    
        //crea combo para 2 personas
        Combo combo2p = new Combo("Combo doble con patatas", "2 hamburguesas doble chesburguer y patatas");
        combo2p.anadeProducto(get(0));
        combo2p.anadeProducto(get(0));
        combo2p.anadeProducto(get(1));
        
        return combo2p;
        
    }

    /**
     * Genera comboPatatasCocacola
     * @return Producto comboPatatasCocacola
     */
    private Producto comboPatatasCocacola() {
    
        //crea combo para 2 personas
        Combo combo1p = new Combo("Combo doble con patatas y cocacola", "hamburguesas doble chesburguer,cocacola y patatas");
        combo1p.anadeProducto(get(0));
        combo1p.anadeProducto(get(0));
        combo1p.anadeProducto(get(1));
        combo1p.anadeProducto(get(3));
        
        return combo1p;
    }
    
    /**
     * Devuelve el producto requerido por el String
     * @param s producto que se pide
     * @return Producto requerido
     */
    public Producto get(int n){
        return carta[n];
    }
    
    /**
     * Encargado de retornar todos los productos que estan en la carta
     * @return String con toda la información.
     */
    public String imprimeCarta(Producto[] carta) {
        
        String sCarta = "\t\t\tCARTA\n";
        
        for (Producto producto : carta) {
            sCarta += producto.getInfo() + "\n\n";
        }
        
        return sCarta;
    }

    /**
     * Devuelve la carta de productos
     * @return carta
     */
    Producto[] getCarta() {
        return carta;
    }
}
